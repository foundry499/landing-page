import React from "react"
import { Link } from "gatsby"
import Icon from "react-remixicon"

const Layout = ({ location, title, description, children, hero }) => {
    let links = (
        <>
            <Link className="btn btn-link text-secondary" to={`/team/`}>
                Team
            </Link>
            <Link className="btn btn-link text-secondary" to={`/blog/`}>
                Blog
            </Link>
            <Link className="btn btn-link text-secondary" to={`/docs/`}>
                Docs
            </Link>
            <Link
                className="btn btn-demo mx-2"
                to={`https://app.foundry499.com`}
            >
                Demo
            </Link>
        </>
    )
    let header = (
        <div>
            <header className="navbar container grid-xl">
                <section className="navbar-section">
                    <Link
                        className="navbar-brand mr-2 text-bold text-large text-secondary"
                        to={`/`}
                    >
                        Foundry499
                    </Link>
                </section>
                <section className="navbar-section hide-sm">{links}</section>
            </header>
            <li className="show-sm">
                <Link className="btn btn-link text-secondary" to={`/team/`}>
                    Team
                </Link>
                <Link className="btn btn-link text-secondary" to={`/blog/`}>
                    Blog
                </Link>
                <Link className="btn btn-link text-secondary" to={`/docs/`}>
                    Docs
                </Link>
                <Link
                    className="btn btn-demo mx-2"
                    to={`https://app.foundry499.com`}
                >
                    Demo
                </Link>
            </li>
        </div>
    )

    if (hero) {
        header = (
            <div>
                {header}
                {hero}
            </div>
        )
    }
    return (
        <div>
            <header className="bg-primary py-2 ">{header}</header>
            <main>
                <div
                    className="container grid-lg"
                    style={{ paddingTop: `5px` }}
                >
                    {children}
                </div>
            </main>
            <footer>
                <section
                    className="section section-footer text-center bg-primary"
                    style={{ paddingTop: 25, paddingBottom: 25 }}
                >
                    <h1>Contact Us</h1>
                    <a
                        href="mailto:tharnadek@hey.com"
                        className="btn btn-lg"
                        style={{ width: 250 }}
                    >
                        <Icon name="mail" type="fill" size="1x" /> Send us an
                        email!
                    </a>
                </section>
                <div className="section section-footer hero bg-gray">
                    <div id="copyright" className="container grid-lg">
                        <p className="text-gray">
                            Designed and built by{" "}
                            <a href="https://tylerharnadek.com  ">
                                Tyler Harnadek
                            </a>{" "}
                            and{" "}
                            <a href="https://jonpavelich.com">Jon Pavelich</a>{" "}
                            for SENG 499 at the University of Victoria, Summer
                            2020.
                        </p>
                        <p className="text-gray">
                            <a href="https://gitlab.com/foundry499/landing-page">
                                Source{" "}
                            </a>
                            ·<a href="https://www.gatsbyjs.org/"> GatsbyJS </a>·
                            <a href="https://picturepan2.github.io/spectre/index.html">
                                {" "}
                                SpectreCSS{" "}
                            </a>
                            ·
                            <a href="https://gitlab.com/foundry-499/landing-page/-/blob/master/LICENSE">
                                {" "}
                                MIT License
                            </a>
                        </p>
                    </div>
                </div>
            </footer>
        </div>
    )
}

export default Layout
