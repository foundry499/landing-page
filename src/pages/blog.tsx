// Gatsby supports TypeScript natively!
import React from "react"
import { PageProps, Link, graphql } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

type Data = {
    site: {
        siteMetadata: {
            title: string
            description: string
        }
    }
    allMarkdownRemark: {
        edges: {
            node: {
                excerpt: string
                frontmatter: {
                    title: string
                    date: string
                }
                fields: {
                    slug: string
                }
            }
        }[]
    }
}

const BlogIndex = ({ data, location }: PageProps<Data>) => {
    const siteTitle = `Blog`
    const siteDescription = `Foundry499's Blog Posts`
    const posts = data.allMarkdownRemark.edges

    return (
        <Layout
            location={location}
            title={siteTitle}
            description={siteDescription}
            hero={
                <div className="hero bg-primary">
                    <div className="hero-body container grid-lg text-center">
                        <h1>
                            <Link
                                style={{
                                    boxShadow: `none`,
                                    color: `inherit`,
                                }}
                                to={`/blog/`}
                            >
                                {siteTitle}
                            </Link>
                        </h1>
                        <p>{siteDescription || ""}</p>
                    </div>
                </div>
            }
        >
            <SEO title="All posts" />
            <div style={{ height: 15 }} />
            {posts.map(({ node }) => {
                const title = node.frontmatter.title || node.fields.slug
                return (
                    <article key={node.fields.slug}>
                        <header>
                            <h3>
                                <Link
                                    style={{ boxShadow: `none` }}
                                    to={node.fields.slug}
                                >
                                    {title}
                                </Link>
                            </h3>
                            <small>{node.frontmatter.date}</small>
                        </header>
                        <section>
                            <p
                                dangerouslySetInnerHTML={{
                                    __html: node.excerpt,
                                }}
                            />
                        </section>
                    </article>
                )
            })}
        </Layout>
    )
}

export default BlogIndex

export const pageQuery = graphql`
    query {
        site {
            siteMetadata {
                title
                description
            }
        }
        allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
            edges {
                node {
                    excerpt
                    fields {
                        slug
                    }
                    frontmatter {
                        date(formatString: "MMMM DD, YYYY")
                        title
                        author
                    }
                }
            }
        }
    }
`
