// Gatsby supports TypeScript natively!
import React from "react"
import { PageProps, Link, graphql } from "gatsby"
import Icon from "react-remixicon"

import Layout from "../components/layout"
import SEO from "../components/seo"

type Data = {
    site: {
        siteMetadata: {
            title: string
            description: string
            contributors: {
                name: string
                github: string
                gitlab: string
                website: string
                photo: string
            }[]
        }
    }
    allMarkdownRemark: {
        edges: {
            node: {
                excerpt: string
                frontmatter: {
                    title: string
                    date: string
                }
                fields: {
                    slug: string
                }
            }
        }[]
    }
}

const TeamIndex = ({ data, location }: PageProps<Data>) => {
    const siteTitle = `Team`
    const siteDescription = `The ${data.site.siteMetadata.title} team.`
    const contributors = data.site.siteMetadata.contributors

    return (
        <Layout
            location={location}
            title={siteTitle}
            description={siteDescription}
            hero={
                <div className="hero bg-primary">
                    <div className="hero-body container grid-lg text-center">
                        <h1>
                            <Link
                                style={{
                                    boxShadow: `none`,
                                    color: `inherit`,
                                }}
                                to={`/blog/`}
                            >
                                {siteTitle}
                            </Link>
                        </h1>
                        <p>{siteDescription || ""}</p>
                    </div>
                </div>
            }
        >
            <SEO title="Team" />

            <div className="columns m-2 p-2">
                {contributors.map(
                    ({ name, photo, github, gitlab, website }) => {
                        return (
                            <div className="column col-4 col-md-auto col-mx-auto p-2">
                                <div className="card bg-gray">
                                    <div className="card-header">
                                        <div className="card-title text-bold h5">
                                            {name}
                                        </div>
                                        <div className="card-subtitle text-gray">
                                            {github}
                                        </div>
                                    </div>
                                    <div className="card-image">
                                        <img
                                            src={"/" + photo}
                                            className="img-responsive"
                                        />
                                    </div>

                                    <div className="card-body text-center bg-gray">
                                        {github && (
                                            <a
                                                href={
                                                    "https://github.com/" +
                                                    github
                                                }
                                            >
                                                <Icon
                                                    name="github"
                                                    type="line"
                                                    size="2x"
                                                />
                                            </a>
                                        )}{" "}
                                        {gitlab && (
                                            <a
                                                href={
                                                    "https://gitlab.com/" +
                                                    gitlab +
                                                    " "
                                                }
                                            >
                                                <Icon
                                                    name="gitlab"
                                                    type="line"
                                                    size="2x"
                                                />
                                            </a>
                                        )}{" "}
                                        {website && (
                                            <a href={website}>
                                                {" "}
                                                <Icon
                                                    name="home-2"
                                                    type="line"
                                                    size="2x"
                                                />
                                            </a>
                                        )}
                                    </div>
                                </div>
                            </div>
                        )
                    }
                )}
            </div>
        </Layout>
    )
}

export default TeamIndex

export const pageQuery = graphql`
    query {
        site {
            siteMetadata {
                title
                description
                contributors {
                    name
                    github
                    gitlab
                    website
                    photo
                }
            }
        }
        allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
            edges {
                node {
                    excerpt
                    fields {
                        slug
                    }
                    frontmatter {
                        date(formatString: "MMMM DD, YYYY")
                        title
                        author
                    }
                }
            }
        }
    }
`
