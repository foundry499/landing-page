import React from "react"
import { graphql } from "gatsby"
import SEO from "../components/seo"
import { RedocStandalone } from "redoc"

const Docs = ({ data, location }) => {
    return (
        <div>
            <SEO title="Docs" />
            <RedocStandalone
                specUrl="https://api.foundry499.com/docs/doc.json"
                options={{
                    theme: {
                        colors: { primary: { main: "#24305E" } },
                        sidebar: {
                            backgroundColor: "#374785",
                            textColor: "#fff",
                        },
                        rightPanel: { backgroundColor: "#24305E" },
                        typography: {
                            fontFamily: "Open Sans",
                            headings: {
                                fontFamily: "Work Sans",
                            },
                            code: {
                                fontFamily: "Fira Code",
                            },
                        },
                    },
                }}
            />
        </div>
    )
}

export default Docs

export const pageQuery = graphql`
    query {
        site {
            siteMetadata {
                title
            }
        }
    }
`
