// Gatsby supports TypeScript natively!
import React from "react"
import { PageProps, Link, graphql } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Icon from "react-remixicon"
type Data = {
    site: {
        siteMetadata: {
            title: string
            description: string
        }
    }
    allMarkdownRemark: {
        edges: {
            node: {
                excerpt: string
                frontmatter: {
                    title: string
                    date: string
                }
                fields: {
                    slug: string
                }
            }
        }[]
    }
}

const Index = ({ data, location }: PageProps<Data>) => {
    const siteTitle = "Manage Azure Workloads With Ease"
    const siteDescription = "We'll do the heavy lifting for you."

    return (
        <Layout
            location={location}
            title={siteTitle}
            description={siteDescription}
            hero={
                <div className="hero bg-primary">
                    <div className="hero-body container grid-lg text-center">
                        <h1>{siteTitle}</h1>
                    </div>
                    <div
                        className="container grid-lg text-center "
                        style={{ paddingTop: 25 }}
                    >
                        <div className="columns">
                            <div className="column col-6">
                                <a href="https://app.foundry499.com">
                                    <img
                                        className="filter-white grow"
                                        src={"/analytics.svg"}
                                        style={{ width: "125px" }}
                                    />
                                    <p
                                        style={{ color: "white" }}
                                        className="py-2"
                                    >
                                        <b>
                                            A human-friendly UI for managing
                                            Jobs on Azure and keeping track of
                                            how they're doing
                                        </b>
                                    </p>
                                    <button className="btn btn-demo text-large">
                                        <Icon
                                            name="reactjs"
                                            type="line"
                                            size="1x"
                                        />{" "}
                                        Web App
                                    </button>
                                </a>
                            </div>
                            <div className="column col-6">
                                <Link to="https://www.foundry499.com/docs">
                                    <img
                                        className="filter-white grow"
                                        src={"/website.svg"}
                                        style={{ width: "125px" }}
                                    ></img>
                                    <p
                                        style={{ color: "white" }}
                                        className="py-2"
                                    >
                                        <b>
                                            A machine-friendly API for managing
                                            and monitoring jobs on the Azure API
                                        </b>
                                    </p>
                                    <button className="btn btn-demo text-large">
                                        <Icon
                                            name="code"
                                            type="line"
                                            size="1x"
                                        />{" "}
                                        API
                                    </button>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            }
        >
            <SEO title="Home" />

            <section className="columns" style={{ paddingBottom: 50 }}>
                <div
                    className=" column col-12 text-center"
                    style={{ paddingTop: 50, paddingBottom: 25 }}
                >
                    <h1>Create & Monitor</h1>
                    <p>
                        Azure VMs are just one click away. Allocate new VMs,
                        deploy containers, and monitor their progress in one
                        step.
                    </p>
                </div>
                <div className="column col-4 col-sm-auto">
                    <h4>Deploy Docker Containers</h4>
                    <p>
                        Docker containers can be deployed from any repository -
                        just provide us with the location, start commands, and
                        environment variables.
                    </p>
                </div>
                <div className="column col-4 col-sm-auto">
                    <h4>Automatically Allocate</h4>
                    <p>
                        Virtual Machines are automatically created to hold as
                        many jobs as possible, right up to your Azure Quota.
                    </p>
                </div>
                <div className="column col-4 col-sm-auto">
                    <h4>Save Time</h4>
                    <p>
                        Spend less time deploying jobs to cloud services and
                        spend more time innovating, creating, and learning.
                    </p>
                </div>
                <div className="column col-4 col-sm-auto">
                    <h4>Logging Built In</h4>
                    <p>All the logs from the container and VM, persisted.</p>
                </div>
                <div className="column col-4 col-sm-auto">
                    <h4>Friendly UI</h4>
                    <p>
                        Our web app makes it easy for anyone to create a new
                        job.
                    </p>
                </div>
                <div className="column col-4 col-sm-auto">
                    <h4>Accessible API</h4>
                    <p>
                        Use the API to create jobs directly from your own
                        software.
                    </p>
                </div>
            </section>
        </Layout>
    )
}

export default Index

export const pageQuery = graphql`
    query {
        site {
            siteMetadata {
                title
                description
            }
        }
    }
`
