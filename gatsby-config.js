module.exports = {
    siteMetadata: {
        title: `Foundry499`,
        description: `Container orchestration on Microsoft Azure`,
        siteUrl: `https://www.foundry499.com/`,
        menuLinks: [
            {
                name: `Team`,
                link: `/team`,
            },
            {
                name: `Home`,
                link: `/home`,
            },
        ],
        contributors: [
            {
                name: `Tyler Harnadek`,
                github: `tharnadek`,
                gitlab: `tharnadek`,
                website: `https://www.tylerharnadek.com`,
                photo: `tyler.jpg`,
            },
            {
                name: `Jon Pavelich`,
                github: `jonpavelich`,
                gitlab: `jonpavelich`,
                website: `https://www.jonpavelich.com`,
                photo: `jon.jpg`,
            },
        ],
    },
    plugins: [
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                path: `${__dirname}/content/blog`,
                name: `blog`,
            },
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                path: `${__dirname}/content/assets`,
                name: `assets`,
            },
        },
        {
            resolve: `gatsby-transformer-remark`,
            options: {
                plugins: [
                    {
                        resolve: `gatsby-remark-images`,
                        options: {
                            maxWidth: 590,
                        },
                    },
                    {
                        resolve: `gatsby-remark-responsive-iframe`,
                        options: {
                            wrapperStyle: `margin-bottom: 1.0725rem`,
                        },
                    },
                    `gatsby-remark-prismjs`,
                    `gatsby-remark-copy-linked-files`,
                    `gatsby-remark-smartypants`,
                ],
            },
        },
        `gatsby-transformer-sharp`,
        `gatsby-plugin-sharp`,
        {
            resolve: `gatsby-plugin-google-analytics`,
            options: {
                //trackingId: `ADD YOUR TRACKING ID HERE`,
            },
        },
        `gatsby-plugin-feed`,
        {
            resolve: `gatsby-plugin-manifest`,
            options: {
                name: `Foundry499 Website`,
                short_name: `Foundry499`,
                start_url: `/`,
                background_color: `#ffffff`,
                theme_color: `#24305E`,
                display: `minimal-ui`,
                icon: `content/assets/icon.png`,
            },
        },
        `gatsby-plugin-react-helmet`,
        `gatsby-plugin-sass`,
        // this (optional) plugin enables Progressive Web App + Offline functionality
        // To learn more, visit: https://gatsby.dev/offline
        // `gatsby-plugin-offline`,
    ],
}
