

## 🚀 Quick start development

1.  **Install NodeJS and yarn.**

    [NodeJS](https://nodejs.org/en/download/)

    [Yarn](https://classic.yarnpkg.com/en/docs/install/#windows-stable)

1.  **Install GatsbyJS.**

    Run the following to install GatsbyJS

    ```shell
      npm install -g gatsby-cli
    ```

1.  **Install yarn dependencies.**

    Run the following to install GatsbyJS

    ```shell
      yarn install
    ```

1.  **Run the development server.**

    Run the following to start the Gatsby development server:

    ```shell
      gatsby develop
    ```

    Connect to http://localhost:8000/ in your browser to inspect the site.

    You can also check out the GraphQL Schema at http://localhost:8000/___graphql

1.  **Create a production build.**

    Run the following to run a build

    ```shell
      gatsby build
    ```
    The output will be static files inside of /public



## 📝 Writing new posts

1.  **Create the folder structure**

    Posts are stored in `/content/blog`. Each post should have a folder with the given date. Multiple posts can be placed into a folder if they have the same date. 

    Posts are formatted in markdown files named according to the post title. Each post must have the following metadata snippet at the very top of the page:

    ```
    ---
    title: My Clever Title
    date: "YYYY-MM-DD"
    author: "MyFirstName MyLastName"
    ---
    ```

    Images can be stored alongside the markdown files in whatever format you like. Gatsby will automatically create responsive images out of them so don't worry about formatting them.

    The structure of a completed post will look like this:

    ```
    .
    / content
      / blog
        /YY-MM-DD
          /my-clever-title.md
          /my-image-1.jpg
          /my-image-2.png
    ```

## ⚙ Site Deployment

This site is hosted on Netlify. All pushes to the repository will be rebuilt and deployed automatically.