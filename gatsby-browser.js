// custom typefaces
import "typeface-work-sans"
import "typeface-open-sans"
import "typeface-fira-code"

import "./src/styles/foundry.scss"
import "./src/styles/prism.css"
import "./node_modules/remixicon/fonts/remixicon.css"
