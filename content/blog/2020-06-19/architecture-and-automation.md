---
title: Architecture and Automation
date: "2020-06-19"
author: "Jon Pavelich"
---

Last week's update detailed building the API in Go. This week we'll dive in to the architecture we've designed, the services we're using, and the automation that makes it tick.

## Architecture

Everything we're building for the Foundry499 projects runs as a container. This makes the architecture pretty straightforward: its only job is to run containers and network everything together.

To start off, here's a high-level diagram that illustrates how everything fits together:

![Architecture Diagram](./architecture-diagram.drawio.png)

- Secure (HTTPS) network requests are shown as a green arrow with a closed lock.
- Insecure (HTTP) network requests are shown as black arrows with an open lock.
- Storage on the persistent disk is shown as an orange arrow.
- Monitoring and control is shown as dotted purple arrows.

### Foundry499 VM

This is an Azure virtual machine running [CentOS 8]. It uses [Systemd] _(purple)_ to start and monitor services, [Caddy] _(blue)_ to receive network requests and forward them to the containers, and [Podman](#podman) _(grey)_ to run the containers _(green)_. It also has a persistent disk _(orange)_ attached to it for data storage.

### Systemd
Systemd runs and monitors services - it starts them when the server starts, and restarts them if they crash. It has both system services and user services: system services are owned by root, while user services are owned by a non-root user.

- Caddy is a system service - it is started by `root`, but runs as a non-privileged user called `caddy`.
- Each container is a separate user service - they are started by a non-privileged user called `podman` and run as this user.

Systemd also handles log collection via Journald. Logs for the system itself, Caddy, and each container are all accessible via the `journalctl` command.

### Caddy

Caddy is a modern web server (written in Go!) that we're using as a reverse proxy. It is responsible for accepting requests from the internet, terminating the TLS connection, and directing the request to the appropriate container. There are a few advantages to using a reverse proxy:
1. Caddy can route requests by hostname, allowing multiple services to share port 443.
1. The public-facing component of the system is a battle-tested web server (that we did not build).
1. TLS certificates are provisioned automatically by Caddy using Let's Encrypt.
1. Caddy automatically upgrades requests to the latest standards - TLS 1.3 and HTTP/2 - whenever possible.
1. Our services only need to support HTTP, since TLS is terminated before a request reaches them.

Caddy runs as a standard system user with limited permissions. A special capability called `CAP_NET_BIND_SERVICE` is given to the binary so it can bind to privileged ports (80 and 443) despite running as a non-privileged user.

### Podman

[Podman] is a container engine that we use to run containers (shown in green on the diagram). It was designed to be a drop-in replacement for Docker. I like it because it is daemonless (meaning it does not require a background daemon like Docker) and rootless (meaning it can run containers without root access) - these are both big security wins.

Podman is running as a standard system user (called `podman`) with limited permissions. The Systemd services that control each container are also run as the `podman` user.

### Containers

There are three containers being run on the server: API, Service Queue, and Database. Tyler [detailed the API previously](../2020-06-12/building-the-api) and the Service Queue is not built yet. The database container is running [PostgreSQL], and has its data stored on the server's persistent storage.

Each container is configured to expose a set of ports to the host system (but not the internet). For example, the API container exposes port `8080` and the database container exposes port `5432`. These ports are used by other containers running on the system to communicate with each other, and by Caddy to forward requests from the Internet to the containers.

## Automation

One of the major goals of this project is to ensure that everything we build can be handed off to another team. The architecture described above is relatively straightforward, but setting it up and maintaining it manually is a lot of work and very difficult for another team to reproduce. Our solution: **automate all the things!** 🎉

The server is configured using Ansible and the container images are built using GitLab CI. I'll detail Ansible this week, and leave GitLab CI for a future update.

### Ansible

[Ansible] is an open-source tool that uses YAML files to describe a series of tasks to apply to a server. This allows us to store our infrastructure as code, which gives us a full history of changes in version control and ensures the server can always be redeployed on a fresh machine.

Ansible includes **modules** that handle the details of most configuration tasks so we can focus on *what needs to be done*, rather than on *how to do it*. These modules are organized together into reusable **roles** that perform all the steps needed for a single purpose (like "bootstrap the system" or "configure a webserver"). These roles are then organized into **playbooks**, which describe the entire state of a server. In my opinion, the two coolest things Ansible does are agentless connections and idempotent deployments.

Being **agentless** means that Ansible doesn't need to install any software on the server being configured - it connects via SSH, and as long as it can locate Python somewhere on the system it is good to go. This is great because there's no manual setup process to get ready to use Ansible - you can point it at a brand new server with SSH access and Ansible can configure absolutely everything.

Being **idempotent** means that Ansible checks the state of the system for each task, and only changes the system if it is not already configured correctly. This means that the first time you run it changes are applied for each and every task, but the second time it will just verify everything is configured correctly and make no changes. Whenever we make a minor modification to the playbook and re-run it, only the steps we modified will make changes to the server.

Ready for an example? Here's a simple task that uses the `systemd` module to enable and start a service called `caddy.service`:

```yaml
- name: "caddy | start and enable systemd service" # just a human-readable name
  become: yes # run this with root privileges
  systemd: # use the systemd module
    name: caddy.service # control the "caddy" service 
    state: started # ensure the service is started
    enabled: yes # ensure the service is enabled
    daemon-reload: yes # reload systemd first to pick up changes to the service file
```

If you run it and this service is not enabled or not running, you'll see the following:

![Ansible task changed](./ansible-changed.png)

If the service was already enabled and running (because you've run this task before), you'll instead see the following:

![Ansible task ok](./ansible-ok.png)

This is idempotency in action. At the end of a playbook run, Ansible will print a summary detailing what it did. For the current Foundry499 playbook we can see that it checked 68 tasks (all of which were already configured correctly) and made no changes:

![Ansible summary](./ansible-summary.png)

I'll write more about automation (including a focus on GitLab CI) in the coming weeks. If you're interested in exploring this topic in more detail in the meantime, go check out [foundry499/infrastructure](https://gitlab.com/foundry499/infrastructure)! I've tried to make the documentation in that repository as clear and understandable as possible.

## Conclusion
We've reviewed the architecture for the Foundry499 VM, described each service that runs on it, and taken a look at some of the automation that ties everything together. Over the next couple weeks we'll be diving in to writing the Service Queue, expanding the API, and starting to provision jobs on Azure.

Thanks for reading, see you next week! 👋

[Ansible]: https://www.ansible.com/
[Caddy]: https://caddyserver.com/
[CentOS 8]: https://www.centos.org/about/
[GitLab CI]: https://about.gitlab.com/ci-cd/
[Podman]: https://podman.io/whatis.html
[PostgreSQL]: https://www.postgresql.org/
[Systemd]: https://systemd.io/