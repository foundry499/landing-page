---
title: Mid-Project Progress Report
date: "2020-07-03"
author: "Tyler Harnadek"
---

This week we spent quite a bit of time writing an update report for our project stakeholders (and not enough time writing code!) so this update will be a bit shorter than usual.

##  Development Update

Recently we've been focused on two things:
- Building the service queue to interact with Azure. This is probably the biggest part of the application, and the least complete so far.
- Designing the UI and wiring it up to the API. This is going pretty quickly.

Interacting with the Service Queue really deserves it's own post, so instead, here's a screenshot from the UI!

![UI Screenshot](./screenshot.png)

Building UI elements is always fun because clicking on things is much more satisfying than running tests in a command line.

### Project Velocity

Just so that no one thinks we're slowing down on the project, here's a commit velocity chart: 

![Project Velocity Stacked Area Chart](./project_velocity.png)

## Next Few Weeks

In the next couple of weeks our plan is really to complete the following goals:

1. Finish the servicequeue's integration with Azure and get some testing done so we can ensure that it's stable
1. Build out the remaining UI elements so that we have MVP functionality
1. Run some real workloads on the platform to prove that it works

Once those goals are done, we can get to the fine-tuning steps.