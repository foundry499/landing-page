---
title: Building an API with Go
date: "2020-06-12"
author: "Tyler Harnadek"
---

In the past few weeks, we've mainly focused on building infrastructure (as code!) and learning how to write apps in Go. Today's update is going to focus on how we're building the REST API component, some of the lessons learned, and hopefully provide a few useful tips for writing Go.

##  App Structure

The first thing to tackle in a fresh project is figuring out where to put things, and Go doesn't enforce a structure. Our folder structure looks like this:

```text
.
+-- cmd/api/
|   +-- main.go <-- app entry point
+-- migrations/
+-- pkg/
|   +-- auth/
|   +-- http/
|   +-- postgres/
|   +-- types/
```

- **cmd**: Every Go app needs an entry point, and this is ours. The `cmd/api/` folder contains just one file - `main.go`. The main file's responsibility is to stitch together the packages, environment variables, and database services so they can all work together.

- **pkg**: The pkg folder is specially named in Go - packages inside of this folder are automatically exported and accessible to other applications. Every folder inside of our pkg folder represents a unique package with it's own namespace (ie `postgres`). All of the files inside the folder are relevant to the package. Because the packages are exported, it is important to build them in a modular and reusable way. 

- **migrations**: Database migration scripts are stored in a top level folder. The migrations are applied programmatically when the application starts (and the database keeps track of which migrations have already been applied). The migration files are numbered so they can be applied in order, and databases can be upgrade and downgraded between any two versions. We keep them separate from the packages.

## Packages

Hopefully it's obvious that the `pkg` folder is where the meat of the application lives! Here's what each package does and a bit about our package organizing structure.

- The `types` package is special - it contains domain types for the application. Domain types are defined as types that are implementation-independent: in our case, we have Users as a domain type, as well as Jobs (for processing tasks).

- Packages are grouped by dependency. The `postgres` package groups together services that rely on the postgres database to get data. Right now we only use postgres as a data source, but this will make it easy to plug in a new service later (such as an Azure data store).

- Test files go alongside the code they test. To test a file named `example.go`, you'd add a file named `example_test.go` with the tests inside. If this gets too messy, maybe it's time to start using more folders.

#### **http**

The `http` package depends on the [gorilla/mux](http://www.gorillatoolkit.org/pkg/mux) web toolkit to implement HTTP routes. It directs traffic, checks user permissions, and generally is the conductor of the whole operation. Creating routes and middleware to operate on the routes is really straightforward with mux; check out the following snippet that implements the logic for logins and logouts:

```go
func getRouter() *mux.Router {
	r := mux.NewRouter()
	// apply logger to all routes
	r.Use(LoggerMiddleware)

	// session router
	r.HandleFunc("/login", LoginHandler).Methods("POST")
	r.HandleFunc("/logout", LogoutHandler).Methods("POST")
	r.HandleFunc("/refresh", RefreshSessionHandler).Methods("POST")
}

```

Our application makes use of sessions to provide user authentication, and the sessions code also lives in `http`. The `http` package is generally responsible for the CRUD (Create/Read/Update/Delete) logic of the application, and the services provided by `postgres` are designed to be as 'dumb' as possible.

#### **auth**

The `auth` package depends on bcrypt and is what we use to store passwords. bcrypt does nearly all the work here - our code is barely more than a wrapper around the bcrypt implementation. We still wrote tests for it though - important to get the password encryption right. 

I included a sample test below. We allow emoji passwords! (this may not be a recommended policy) 

```go
func TestEmojiPasswordHash(t *testing.T) {
	p := "🔑🤣🚀" // unbelievable that this works
	hp, err := HashPassword(p)
	if err != nil {
		t.Fatalf("HashPassword error: %s", err)
	}
	fmt.Print(hp)
	if CheckPasswordHash(p, hp) != true {
		t.Errorf("%v should hash %s correctly", hp, p)
	}

	bp := "someotherpass"
	if CheckPasswordHash(bp, hp) != false {
		t.Errorf("%v and %s should not match", hp, bp)
	}
}
```

bcrypt is cool because it does the salting and hashing for you, and stores the salt as a part of the output string so you don't need extra database columns.

#### **postgres**

`postgres` defines data access services on the postgres database - users and jobs, currently. The services implement interfaces that are defined in `types`. Below is the whole UserService interface (defined in `types`). The UserService is implemented in `postgres/user.go` and implements the functions defined below as PostgreSQL queries. 

```go 
package types
// UserService implements the User domain type
type UserService interface {
	GetUserByID(id int) (*User, error)
	GetUserByEmail(email string) (*User, error)
	GetAllUsers() ([]*User, error)
	CreateUser(u *User) error
	DeleteUser(id int) error
	UpdateUserByID(id int, u *User) error
}
```

Overall the services are designed to be database-agnostic, so for example if you wanted to switch to MongoDB, Cassandra, or some kind of local file store you'd be able to do so easily by just implementing these specific functions. 

It also makes the services a lot easier to test - we can just mock out these database services and then do unit tests without a database.

## Conclusion

We decided to start with the API as a learning experience - we're comfortable writing APIs, but had a lot to learn about Go. We've got lots of ideas going forward about how to best develop the ServiceQueue component of the system now that we have a bit of experience with Go. 

Next week look forward to a post from Jon next week about architecture and automation!