---
title: Ready to Go
date: "2020-06-07"
author: "Tyler Harnadek"
---

We made lots of progress on the Foundry project this week! Here's a quick list of hits:

- **Infrastructure as Code**: Jon has been working on automating container deployments for our API and database. 
Everything including container deployment and database migration is automated and working!

- **Services**: I've been working at creating services for the API portion, including creating database types and defining REST endpoints.

- **Azure Research**: We've both been exploring Azure's cloud offerings and determining how to best maximize our use of them. Lots more to come about this in future updates!

### Container Support  ![docker](./docker.gif)

For this project we're developing our services with Go. We've noticed that Go doesn't completely support Windows development and Go environments can be tricky to deal with, so we're doing our development in Containers!

- Jon likes to use **`podman`** and I am more familiar with **`docker`** so our infrastructure is being built to support both.

- For development, we spin up containers for each of our services, perfectly replicating the production environment.

- database migrations are performed at every launch so we can ensure that they actually work! No broken migrations 😭. 

- We're taking advantage of Go libraries to support hot reloading of code inside the containers so we don't even have to restart them as we develop

In production, the code is deployed using the same container architecture as development so we can ensure that the environments match exactly.


### Next Week 

For next week, I'll be detailing how we've decided to structure our Go apps with some code examples!