---
title: Off To The Races
date: "2020-05-29"
author: "Tyler Harnadek"
---

It's the end of May and we're underway! Thanks for checking out our dev blog. 
I'm going to try to post an update every week that summarizes what we've done, 
where we're going, and hopefully shares some of our learning.

First off, a quick note about our team: I'm Tyler Harnadek and along with
Jon Pavelich, we're the developers working on this project. Yvonne Coady has 
agreed to supervise us this semester and has already helped us to find some 
great project requirements. 

### Project Update

First off, we've been discussing high-level architecture for the system, as well
as deciding on a cost-effective deployment plan. Here's a diagram of what we 
think will be effective - later on, we might choose to add a few more
components, for example for logging and monitoring.

![Deployment Diagram](./deployment-diagram.png)

**Foundry499 API**: REST API communicates with the web application and provides
an interface for other applications to interact with the system.

**Foundry499 Web Interface**: Web App built to provide overview of system status
and allow users to add new jobs to the system.

**ServiceQueue Container**: This component accesses job info from the database,
and communicates with Azure to deploy containers as needed.

**PostgreSQL Database**: Stores job info

**Docker Container Repository**: Our containers are being stored on the GitLab
container repository, but any container repository will work as long as it has
a standard authentication method.

**Azure Batch/VMSS**: We still need to do an analysis to determine which is 
going to be cheaper, but we'll be using Azure VMs to perform compute
tasks.

#### Tasks Completed So Far

So far we've done some of the preliminary work - I've been working on getting
this website set up, creating repositories for our code [on GitLab](https://gitlab.com/foundry-499),
and Jon has been working on creating a Docker container for initial testing.

#### Upcoming Tasks

We're going to use GitLab extensively as a dev tool for this project and we'll
be tracking goals in our GitLab [issue tracker](https://gitlab.com/groups/foundry-499/-/issues).

Next week, we're hoping to have most of our services deployed and ready for
some heavy development on the ServiceQueue.

A lot of this project will also be researching Azure's options, and this will
be underway next week as well.

### Cheers

Thanks for reading this far! We'll check in again next week.

